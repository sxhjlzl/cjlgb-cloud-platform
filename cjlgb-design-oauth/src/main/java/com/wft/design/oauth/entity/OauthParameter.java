package com.wft.design.oauth.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wft.design.common.security.bean.LoginParameter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author WFT
 * @date 2019/12/30
 * description:
 */
@Getter
@Setter
@NoArgsConstructor
public class OauthParameter extends LoginParameter {

    public interface OauthValidation{

    }

    /**
     * 授权方式,必填
     */
    @JsonProperty(value = "grant_type")
    @NotBlank(message = "授权方式不能为空",groups = OauthValidation.class)
    private String grantType;

    /**
     * 客户端Id,必填
     */
    @JsonProperty(value = "client_id")
    private String clientId;

    /**
     * 客户端密钥,必填
     */
    @JsonProperty(value = "client_secret")
    private String clientSecret;

    /**
     * 授权码，授权码模式时必填
     */
    private String code;

    /**
     * 刷新令牌
     */
    @JsonProperty(value = "refresh_token")
    private String refreshToken;

}
