package com.wft.design.oauth.service;

import com.wft.design.oauth.entity.OauthClient;
import com.wft.design.oauth.entity.OauthGrantCode;

/**
 * @author WFT
 * @date 2019/12/22
 * description: 授权码CRUD接口
 */
public interface GrantCodeService {

    /**
     * 根据Id获取授权码信息,并删除授权码
     * @param id 唯一标识
     * @return com.wft.design.oauth.entity.OauthGrantCode
     */
    OauthGrantCode selectById(String id);

    /**
     * 生成授权码
     * @param accountId 账号信息
     * @param client oauth2客户端信息
     * @return java.lang.Long
     */
    Long createGrantCode(Long accountId, OauthClient client);

}
