package com.wft.design.oauth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.oauth.entity.OauthClientApplyRecord;

/**
 * @author WFT
 * @date 2020/4/2
 * description:
 */
public interface OauthClientApplyRecordMapper extends BaseMapper<OauthClientApplyRecord> {

}
