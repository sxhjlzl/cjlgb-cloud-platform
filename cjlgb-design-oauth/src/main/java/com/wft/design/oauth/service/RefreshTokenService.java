package com.wft.design.oauth.service;

import com.wft.design.common.security.bean.Authentication;

/**
 * @author WFT
 * @date 2019/12/30
 * description:
 */
public interface RefreshTokenService {

    /**
     * 生成RefreshToken
     * @param authentication 认证信息
     * @return com.wft.design.oauth.entity.OauthRefreshToken
     */
    String generateRefreshToken(Authentication authentication);

    /**
     * 根据刷新令牌获取凭证信息
     * @param refreshToken 刷新令牌
     * @return com.wft.design.common.security.bean.Authentication
     */
    Authentication getAuthentication(String refreshToken);

}
