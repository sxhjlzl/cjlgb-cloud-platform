package com.wft.design.oauth.service.impl;

import com.wft.design.common.mybatis.service.impl.BaseServiceImpl;
import com.wft.design.oauth.entity.OauthClientApplyRecord;
import com.wft.design.oauth.mapper.OauthClientApplyRecordMapper;
import com.wft.design.oauth.service.OauthClientApplyRecordService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author WFT
 * @date 2020/4/2
 * description:
 */
@Service
@RequiredArgsConstructor
public class OauthClientApplyRecordServiceImpl extends BaseServiceImpl<OauthClientApplyRecordMapper, OauthClientApplyRecord>
        implements OauthClientApplyRecordService {



}
