package com.wft.design.oauth.entity;

import com.wft.design.common.core.bean.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author WFT
 * @date 2019/12/22
 * description: 授权码
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class OauthGrantCode extends BaseEntity {

    /**
     * 账号Id
     */
    private Long accountId;

    /**
     * 客户端信息
     */
    private OauthClient client;

}
