package com.wft.design.oauth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.oauth.entity.OauthClient;

/**
 * @author WFT
 * @date 2019/12/22
 * description:
 */
public interface OauthClientMapper extends BaseMapper<OauthClient> {


}
