package com.wft.design.oauth.service.impl;

import com.wft.design.common.mybatis.service.impl.BaseServiceImpl;
import com.wft.design.oauth.entity.OauthClient;
import com.wft.design.oauth.mapper.OauthClientMapper;
import com.wft.design.oauth.service.OauthClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author WFT
 * @date 2019/12/22
 * description:
 */
@Service
@RequiredArgsConstructor
public class OauthClientServiceImpl extends BaseServiceImpl<OauthClientMapper, OauthClient>
        implements OauthClientService {

    @Override
    public OauthClient selectById(String id) {
        return this.getById(id);
    }
}
