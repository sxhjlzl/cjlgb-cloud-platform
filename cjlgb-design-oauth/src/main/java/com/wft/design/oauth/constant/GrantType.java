package com.wft.design.oauth.constant;

/**
 * @author WFT
 * @date 2019/12/23
 * description: 授权方式
 */
public enum GrantType {

    /**
     * 密码模式
     */
    password,

    /**
     * 授权码模式
     */
    authorization_code

}
