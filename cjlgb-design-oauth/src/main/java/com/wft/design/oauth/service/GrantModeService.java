package com.wft.design.oauth.service;

import com.wft.design.common.security.bean.Authentication;
import com.wft.design.oauth.entity.OauthParameter;

/**
 * @author WFT
 * @date 2019/12/19
 * description: 认证模式接口
 */
public interface GrantModeService {

    /**
     * 模式名称
     * @return java.lang.String
     */
    String getName();

    /**
     * 校验认证信息
     * @param parameter 请求参数
     * @return com.wft.design.common.security.bean.Authentication
     */
    Authentication checking(OauthParameter parameter);
}
