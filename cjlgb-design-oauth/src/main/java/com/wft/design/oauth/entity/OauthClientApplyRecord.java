package com.wft.design.oauth.entity;

import com.wft.design.common.core.bean.BaseEntity;
import com.wft.design.common.core.validation.AddValidation;
import com.wft.design.common.core.validation.EditValidation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author WFT
 * @date 2020/4/2
 * description: Oauth2客户端申请记录
 */
@Getter
@Setter
@NoArgsConstructor
public class OauthClientApplyRecord extends BaseEntity {

    /**
     * 授权方式：{ authorization_code ： 授权码模式, password ： 密码模式  }
     */
    @NotBlank(message = "授权方式不能为空",groups = { AddValidation.class, EditValidation.class })
    private String grantType;

    /**
     * 应用名称
     */
    @NotBlank(message = "应用名称不能为空",groups = { AddValidation.class, EditValidation.class })
    private String appName;

    /**
     * 应用Logo
     */
    @NotBlank(message = "应用Logo不能为空",groups = { AddValidation.class, EditValidation.class })
    private String appLogo;

    /**
     * 应用描述
     */
    @NotBlank(message = "应用描述不能为空",groups = { AddValidation.class, EditValidation.class })
    private String appDesc;

    /**
     * 回调地址
     */
    @NotBlank(message = "回调地址不能为空",groups = { AddValidation.class, EditValidation.class })
    private String callback;

    /**
     * 授权标识:{ -1 : 拒绝, 0 : 通过 }
     */
    private String grantFlag;

    /**
     * 拒绝理由
     */
    private String rejectReason;

    /**
     * 申请人名称
     */
    private String applicantName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

}
