package com.wft.design.oauth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * @author WFT
 * @date 2019/12/30
 * description:
 */
@SpringCloudApplication
@MapperScan(value = "com.wft.design.oauth.mapper")
public class OauthApplication {

    public static void main(String[] args) {
        SpringApplication.run(OauthApplication.class,args);
    }

}
