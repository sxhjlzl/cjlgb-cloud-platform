package com.wft.design.oauth.service.impl;

import com.wft.design.common.mybatis.service.impl.BaseServiceImpl;
import com.wft.design.oauth.entity.OauthAccount;
import com.wft.design.oauth.mapper.OauthAccountMapper;
import com.wft.design.oauth.service.OauthAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author WFT
 * @date 2019/12/20
 * description:
 */
@Service
@RequiredArgsConstructor
public class OauthAccountServiceImpl extends BaseServiceImpl<OauthAccountMapper, OauthAccount>
        implements OauthAccountService {

    @Override
    public OauthAccount selectById(Long accountId) {
        return this.getById(accountId);
    }
}
