package com.wft.design.oauth.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wft.design.common.mybatis.service.BaseService;
import com.wft.design.oauth.entity.OauthAccount;
import org.springframework.util.DigestUtils;

/**
 * @author WFT
 * @date 2019/12/20
 * description:
 */
public interface OauthAccountService extends BaseService<OauthAccount> {

    /**
     * 根据Id获取账号信息
     * @param accountId 账号Id
     * @return com.wft.design.oauth.entity.OauthAccount
     */
    OauthAccount selectById(Long accountId);

    /**
     * 根据用户名获取账号信息
     * @param username 用户名
     * @return com.wft.design.oauth.entity.OauthAccount
     */
    default OauthAccount selectByName(String username){
        return this.getOne(Wrappers.<OauthAccount>lambdaQuery().eq(OauthAccount::getUsername,username));
    }

    /**
     * 明文密码加密
     * @param plaintext 明文密码
     * @param salt 随机盐
     * @return java.lang.String
     */
    default String encryption(String plaintext, String salt){
        plaintext += salt;
        return DigestUtils.md5DigestAsHex(plaintext.getBytes());
    }

}
