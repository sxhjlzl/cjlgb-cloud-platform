package com.wft.design.oauth.factory;

import com.wft.design.oauth.service.GrantModeService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author WFT
 * @date 2019/12/20
 * description: 授权策略工厂
 */
@Service
public class GrantModeStrategyFactory implements ApplicationContextAware {

    private static Map<String, GrantModeService> SERVICE_MAP;

    /**
     * 根据授权方式获取对应的授权模式
     * @param grantType 授权方式
     * @return com.wft.design.oauth.service.GrantModeService
     */
    public GrantModeService getGrantModeService(String grantType){
        return SERVICE_MAP.get(grantType);
    }

    @Override
    public void setApplicationContext(@NonNull ApplicationContext context) throws BeansException {
        Map<String, GrantModeService> map = context.getBeansOfType(GrantModeService.class);
        SERVICE_MAP = new HashMap<>(map.size());
        map.forEach((beanName,bean) -> SERVICE_MAP.put(bean.getName(),bean));
    }
}
