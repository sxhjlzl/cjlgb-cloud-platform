package com.wft.design.oauth.controller;

import com.wft.design.common.security.annotation.PreAuthorize;
import com.wft.design.common.security.service.AuthenticationManager;
import com.wft.design.oauth.entity.OauthAccount;
import com.wft.design.oauth.service.OauthAccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author WFT
 * @date 2019/12/24
 * description: Oauth2账号控制器
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/oauth/account")
public class OauthAccountController {

    private final AuthenticationManager authenticationManager;
    private final OauthAccountService accountService;

    /**
     * 获取当前用户信息
     * @return com.wft.design.oauth.entity.OauthAccount
     */
    @PreAuthorize
    @GetMapping(value = "/getMyInfo")
    public OauthAccount getAccount(){
        Long accountId = this.authenticationManager.getAuthentication().getAccountId();
        return this.accountService.selectById(accountId);
    }


}
