package com.wft.design.oauth.cfgbean;

import com.wft.design.oauth.entity.OauthGrantCode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author WFT
 * @date 2019/12/22
 * description: Redis模板配置
 */
@Configuration
public class RedisTemplateConfiguration {

    @Bean
    public RedisTemplate<Object, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory){
        RedisTemplate<Object, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redisConnectionFactory);
        return template;
    }

    @Bean
    public RedisTemplate<String, OauthGrantCode> grantCodeRedisTemplate(RedisConnectionFactory factory){
        RedisTemplate<String, OauthGrantCode> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        return template;
    }

}
