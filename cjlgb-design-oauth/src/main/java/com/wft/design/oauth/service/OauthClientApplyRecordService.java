package com.wft.design.oauth.service;

import com.wft.design.common.mybatis.service.BaseService;
import com.wft.design.oauth.entity.OauthClientApplyRecord;

/**
 * @author WFT
 * @date 2020/4/2
 * description:
 */
public interface OauthClientApplyRecordService extends BaseService<OauthClientApplyRecord> {


}
