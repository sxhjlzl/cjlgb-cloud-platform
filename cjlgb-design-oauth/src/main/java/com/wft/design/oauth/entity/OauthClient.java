package com.wft.design.oauth.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wft.design.common.core.bean.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * @author WFT
 * @date 2019/12/22
 * description: Oauth2 客户端信息
 */
@Getter
@Setter
@NoArgsConstructor
public class OauthClient extends BaseEntity {

    /**
     * 客户端密钥
     */
    @JsonIgnore
    private String secret;

    /**
     * 授权方式：{ authorization_code ： 授权码模式, password ： 密码模式  }
     */
    private String grantType;

    /**
     * 应用名称
     */
    private String appName;

    /**
     * 应用Logo
     */
    private String appLogo;

    /**
     * 应用描述
     */
    private String appDesc;

    /**
     * 回调地址
     */
    private String callback;

    /**
     * 锁定标记:{ -1 : 锁定, 0 : 正常 }
     */
    private Integer lockFlag;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

}
