package com.wft.design.oauth.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wft.design.common.core.bean.BaseEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author WFT
 * @date 2019/12/20
 * description: Oauth2 账号信息
 */
@Getter
@Setter
@NoArgsConstructor
public class OauthAccount extends BaseEntity {

    /**
     * 登陆名称
     */
    private String username;

    /**
     * 登陆密码：md5(明文 + 随机盐)
     */
    @JsonIgnore
    private String password;

    /**
     * 锁定标记：{ -1:锁定,0:正常 }
     */
    private Integer lockFlag;

}
