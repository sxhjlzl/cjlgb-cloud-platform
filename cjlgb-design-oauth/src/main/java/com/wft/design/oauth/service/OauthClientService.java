package com.wft.design.oauth.service;

import com.wft.design.common.mybatis.service.BaseService;
import com.wft.design.oauth.entity.OauthClient;

/**
 * @author WFT
 * @date 2019/12/22
 * description:
 */
public interface OauthClientService extends BaseService<OauthClient> {

    /**
     * 根据Id获取客户端信息
     * @param id 客户端Id
     * @return com.wft.design.oauth.entity.OauthClient
     */
    OauthClient selectById(String id);

}
