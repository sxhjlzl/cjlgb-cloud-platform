package com.wft.design.oauth.service.impl;

import com.wft.design.common.security.bean.AccessToken;
import com.wft.design.common.security.bean.Authentication;
import com.wft.design.common.security.service.AccessTokenService;
import com.wft.design.oauth.service.RefreshTokenService;
import com.wft.design.oauth.service.TokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author WFT
 * @date 2020/1/2
 * description:
 */
@Service
@RequiredArgsConstructor
public class TokenServiceImpl implements TokenService {

    private final RefreshTokenService refreshTokenService;
    private final AccessTokenService accessTokenService;

    @Override
    public AccessToken generateToken(Authentication authentication,Integer rememberMe) {
        //  生成RefreshToken
        String refreshToken = null;
        if (null != rememberMe){
            refreshToken = this.refreshTokenService.generateRefreshToken(authentication);
        }
        //  生成AccessToken
        return this.accessTokenService.generateAccessToken(authentication,refreshToken);
    }
}
