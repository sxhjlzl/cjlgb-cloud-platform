package com.wft.design.oauth.controller;

import com.wft.design.common.security.annotation.PreAuthorize;
import com.wft.design.oauth.entity.OauthClient;
import com.wft.design.oauth.service.OauthClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author WFT
 * @date 2019/12/22
 * description: Oauth客户端控制器
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/oauth/client")
public class OauthClientController {

    private final OauthClientService clientService;

    @PreAuthorize
    @GetMapping(value = "/{clientId}")
    public OauthClient getClient(@PathVariable(value = "clientId") String clientId){
        OauthClient client = this.clientService.selectById(clientId);
        Assert.notNull(client,"客户端Id不存在");
        return client;
    }

}
