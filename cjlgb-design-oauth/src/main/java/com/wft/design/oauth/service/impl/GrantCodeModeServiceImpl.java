package com.wft.design.oauth.service.impl;

import com.wft.design.common.security.bean.Authentication;
import com.wft.design.oauth.constant.GrantType;
import com.wft.design.oauth.entity.OauthClient;
import com.wft.design.oauth.entity.OauthGrantCode;
import com.wft.design.oauth.entity.OauthParameter;
import com.wft.design.oauth.service.GrantCodeService;
import com.wft.design.oauth.service.GrantModeService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.DigestUtils;

import java.util.ArrayList;

/**
 * @author WFT
 * @date 2019/12/30
 * description: 授权码模式策略
 */
@Service
@RequiredArgsConstructor
public class GrantCodeModeServiceImpl implements GrantModeService {

    private final GrantCodeService grantCodeService;

    @Override
    public String getName() {
        return GrantType.authorization_code.toString();
    }

    @Override
    public Authentication checking(OauthParameter parameter) {
        //  校验授权码是否为空
        Assert.hasText(parameter.getCode(),"授权码不能为空");
        //  根据授权码获取客户端与账号信息
        OauthGrantCode grantCode = this.grantCodeService.selectById(parameter.getCode());
        Assert.notNull(grantCode,"无效的授权码");
        //  校验客户端Id
        OauthClient client = grantCode.getClient();
        Assert.isTrue(String.valueOf(client.getId()).equals(parameter.getClientId()),"客户端Id与授权码不匹配");
        //  校验客户端密钥
        String cipherText = DigestUtils.md5DigestAsHex((client.getSecret() + parameter.getCode()).getBytes());
        Assert.isTrue(cipherText.equals(parameter.getClientSecret()),"客户端密钥不正确");
        //  TODO 待实现 获取客户端权限列表
        return new Authentication(grantCode.getAccountId(), new ArrayList<>(0));
    }
}
