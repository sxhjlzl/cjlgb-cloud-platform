package com.wft.design.oauth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.oauth.entity.OauthAccount;

/**
 * @author WFT
 * @date 2019/12/20
 * description:
 */
public interface OauthAccountMapper extends BaseMapper<OauthAccount> {
}
