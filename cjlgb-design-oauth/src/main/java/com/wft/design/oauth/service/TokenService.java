package com.wft.design.oauth.service;

import com.wft.design.common.security.bean.AccessToken;
import com.wft.design.common.security.bean.Authentication;

/**
 * @author WFT
 * @date 2020/1/2
 * description:
 */
public interface TokenService {

    /**
     * 生成Token
     * @param authentication 认证信息
     * @param rememberMe 不为空时,生成RefreshToken
     * @return com.wft.design.common.security.bean.AccessToken
     */
    AccessToken generateToken(Authentication authentication,Integer rememberMe);

}
