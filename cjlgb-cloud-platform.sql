DROP DATABASE IF EXISTS `cjlgb_cloud_platform`;
CREATE DATABASE  `cjlgb_cloud_platform` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_bin;

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

USE `cjlgb_cloud_platform`;

DROP TABLE IF EXISTS `oauth_account`;
CREATE TABLE `oauth_account` (
  `id` bigint(20) NOT NULL,
  `username` varchar(50) DEFAULT NULL COMMENT '登陆名称',
  `password` varchar(255) DEFAULT NULL COMMENT '登陆密码：md5(明文 + 随机盐)',
  `lock_flag` tinyint(1) DEFAULT NULL COMMENT '锁定标记：{ -1:锁定,0:正常 }',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='oauth2账号';

DROP TABLE IF EXISTS `oauth_client`;
CREATE TABLE `oauth_client` (
  `id` bigint(20) NOT NULL,
  `secret` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '客户端密钥',
  `grant_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '授权方式：{ authorization_code ： 授权码模式, password ： 密码模式  }',
  `app_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用名称',
  `app_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用Logo',
  `app_desc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用描述',
  `callback` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '回调地址',
  `lock_flag` tinyint(1) DEFAULT NULL COMMENT '锁定标记:{ -1 : 锁定, 0 : 正常 }',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='oauth2客户端';

DROP TABLE IF EXISTS `oauth_client_apply_record`;
CREATE TABLE `oauth_client_apply_record` (
  `id` bigint(20) NOT NULL,
  `grant_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '授权方式：{ authorization_code ： 授权码模式, password ： 密码模式  }',
  `app_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用名称',
  `app_logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用Logo',
  `app_desc` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '应用描述',
  `callback` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '回调地址',
  `grant_flag` tinyint(1) DEFAULT NULL COMMENT '授权标识:{ -1 : 拒绝, 0 : 通过 }',
  `reject_reason` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '拒绝理由',
  `applicant_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '申请人名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='oauth2客户端申请记录表';

DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `dept_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modify_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统部门表';

DROP TABLE IF EXISTS `sys_gateway_route`;
CREATE TABLE `sys_gateway_route` (
  `id` bigint(20) NOT NULL,
  `route_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '路由名称',
  `predicates` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '谓词',
  `filters` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '过滤器',
  `uri` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '请求URI',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modify_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统网关路由';

INSERT INTO `sys_gateway_route` (`id`, `route_name`, `predicates`, `filters`, `uri`, `create_time`, `last_modify_time`) VALUES ('1182154748348878851', '用户管理系统', '[{\"name\":\"Path\",\"args\":{\"pattern\":\"/cjlgb-upms/**\"}}]', '[{\"name\":\"StripPrefix\",\"args\":{\"_genkey_0\":\"1\"}}]', 'lb://cjlgb-design-upms', '2019-10-11 20:43:18', '2019-10-29 03:35:48');
INSERT INTO `sys_gateway_route` (`id`, `route_name`, `predicates`, `filters`, `uri`, `create_time`, `last_modify_time`) VALUES ('1182154748348878852', '账号认证服务', '[{\"name\":\"Path\",\"args\":{\"pattern\":\"/cjlgb-oauth/**\"}}]', '[{\"name\":\"StripPrefix\",\"args\":{\"_genkey_0\":\"1\"}}]', 'lb://cjlgb-design-oauth', '2019-10-11 08:13:55', '2020-02-06 20:03:16');
INSERT INTO `sys_gateway_route` (`id`, `route_name`, `predicates`, `filters`, `uri`, `create_time`, `last_modify_time`) VALUES ('1182154748348878853', '系统配置服务', '[{\"name\":\"Path\",\"args\":{\"pattern\":\"/cjlgb-system/**\"}}]', '[{\"name\":\"StripPrefix\",\"args\":{\"_genkey_0\":\"1\"}}]', 'lb://cjlgb-design-system-biz', '2019-10-11 08:14:02', '2020-02-06 20:03:24');

DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '菜单名称',
  `permission` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `indexes` varchar(255) DEFAULT NULL COMMENT '索引',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `path` varchar(255) DEFAULT NULL COMMENT '前端页面路径',
  `sort` int(255) DEFAULT NULL COMMENT '排序字段',
  `pid` bigint(20) DEFAULT NULL COMMENT '父级菜单Id',
  `type` tinyint(1) DEFAULT NULL COMMENT '类型：{ 1：菜单，2：按钮 }',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modify_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统菜单表';

INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237950824045367298', '权限管理', 'upms', '1237950824045367298/', 'safety', NULL, '1', '0', '1', '2020-03-12 11:57:31', '2020-03-12 16:35:58');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237950932010946561', '系统配置', 'sys', '1237950932010946561/', 'setting', NULL, '2', '0', '1', '2020-03-12 11:57:57', '2020-03-12 12:21:00');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237951196772192258', '菜单管理', 'upms.menu', '1237950824045367298/1237951196772192258/', 'bars', '/upms/menu', '1', '1237950824045367298', '1', '2020-03-12 11:59:00', '2020-03-12 16:36:15');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237951427660238850', '添加', 'upms.menu.add', '1237950824045367298/1237951196772192258/1237951427660238850/', NULL, NULL, '1', '1237951196772192258', '2', '2020-03-12 11:59:55', '2020-03-12 16:36:41');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237951505313583106', '删除', 'upms.menu.delete', '1237950824045367298/1237951196772192258/1237951505313583106/', NULL, NULL, '2', '1237951196772192258', '2', '2020-03-12 12:00:14', '2020-03-12 16:36:49');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237951580249018370', '修改', 'upms.menu.edit', '1237950824045367298/1237951196772192258/1237951580249018370/', NULL, NULL, '3', '1237951196772192258', '2', '2020-03-12 12:00:31', '2020-03-12 16:37:04');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237951651594129409', '查询', 'upms.menu.query', '1237950824045367298/1237951196772192258/1237951651594129409/', NULL, NULL, '4', '1237951196772192258', '2', '2020-03-12 12:00:48', '2020-03-12 16:36:56');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237951869467250689', '角色管理', 'upms.role', '1237950824045367298/1237951869467250689/', 'team', '/upms/role', '2', '1237950824045367298', '1', '2020-03-12 12:01:40', '2020-03-12 16:36:25');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237951925180190722', '添加', 'upms.role.add', '1237950824045367298/1237951869467250689/1237951925180190722/', NULL, NULL, '1', '1237951869467250689', '2', '2020-03-12 12:01:54', '2020-03-12 16:37:22');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237951978548514818', '删除', 'upms.role.delete', '1237950824045367298/1237951869467250689/1237951978548514818/', NULL, NULL, '2', '1237951869467250689', '2', '2020-03-12 12:02:06', '2020-03-12 16:37:48');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237952029719023617', '修改', 'upms.role.edit', '1237950824045367298/1237951869467250689/1237952029719023617/', NULL, NULL, '3', '1237951869467250689', '2', '2020-03-12 12:02:19', '2020-03-12 16:37:40');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237952082474979329', '查询', 'upms.role.query', '1237950824045367298/1237951869467250689/1237952082474979329/', NULL, NULL, '4', '1237951869467250689', '2', '2020-03-12 12:02:31', '2020-03-12 16:37:31');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237952165379592194', '用户管理', 'upms.user', '1237950824045367298/1237952165379592194/', 'user', '/upms/user', '3', '1237950824045367298', '1', '2020-03-12 12:02:51', '2020-03-13 14:53:05');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237952339959107585', '添加', 'upms.user.add', '1237950824045367298/1237952165379592194/1237952339959107585/', NULL, NULL, '1', '1237952165379592194', '2', '2020-03-12 12:03:33', '2020-03-12 16:38:03');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237952502589050881', '删除', 'upms.user.delete', '1237950824045367298/1237952165379592194/1237952502589050881/', NULL, NULL, '2', '1237952165379592194', '2', '2020-03-12 12:04:11', '2020-03-12 16:38:12');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237952663654518785', '修改', 'upms.user.edit', '1237950824045367298/1237952165379592194/1237952663654518785/', NULL, NULL, '3', '1237952165379592194', '2', '2020-03-12 12:04:50', '2020-03-12 16:38:23');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237953401772331009', '查询', 'upms.user.query', '1237950824045367298/1237952165379592194/1237953401772331009/', NULL, NULL, '4', '1237952165379592194', '2', '2020-03-12 12:07:46', '2020-03-12 16:38:32');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237953594093752321', '动态路由', 'sys.gateway', '1237950932010946561/1237953594093752321/', 'global', '/system/route', '1', '1237950932010946561', '1', '2020-03-12 12:08:32', '2020-03-12 16:34:37');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237967795361177601', '添加', 'sys.gateway.add', '1237950932010946561/1237953594093752321/1237967795361177601/', NULL, NULL, '1', '1237953594093752321', '2', '2020-03-12 13:04:57', '2020-03-12 13:04:57');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237967907151962114', '删除', 'sys.gateway.delete', '1237950932010946561/1237953594093752321/1237967907151962114/', NULL, NULL, '2', '1237953594093752321', '2', '2020-03-12 13:05:24', '2020-03-12 13:05:24');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237967962218979329', '修改', 'sys.gateway.edit', '1237950932010946561/1237953594093752321/1237967962218979329/', NULL, NULL, '3', '1237953594093752321', '2', '2020-03-12 13:05:37', '2020-03-12 13:05:37');
INSERT INTO `sys_menu` (`id`, `name`, `permission`, `indexes`, `icon`, `path`, `sort`, `pid`, `type`, `create_time`, `last_modify_time`) VALUES ('1237968014773608449', '查询', 'sys.gateway.query', '1237950932010946561/1237953594093752321/1237968014773608449/', NULL, NULL, '4', '1237953594093752321', '2', '2020-03-12 13:05:50', '2020-03-12 13:05:50');

DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `role_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '角色名称',
  `role_describe` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '描述',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `last_modify_time` datetime DEFAULT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统角色表';

INSERT INTO `sys_role` (`id`, `role_name`, `role_describe`, `create_time`, `last_modify_time`) VALUES ('1225427677895213057', '超级管理员', '所有权限', '2020-02-06 22:35:00', '2020-02-06 22:35:32');

DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色Id',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='角色菜单关联表';

INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237950824120864770', '1225427677895213057', '1237950824045367298');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237950932078055426', '1225427677895213057', '1237950932010946561');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237951196830912514', '1225427677895213057', '1237951196772192258');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237951427718959106', '1225427677895213057', '1237951427660238850');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237951505397469186', '1225427677895213057', '1237951505313583106');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237951580311932929', '1225427677895213057', '1237951580249018370');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237951651673821186', '1225427677895213057', '1237951651594129409');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237951869555331074', '1225427677895213057', '1237951869467250689');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237951925255688193', '1225427677895213057', '1237951925180190722');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237951978607235073', '1225427677895213057', '1237951978548514818');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237952029769355265', '1225427677895213057', '1237952029719023617');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237952082533699586', '1225427677895213057', '1237952082474979329');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237952165429923842', '1225427677895213057', '1237952165379592194');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237952340047187970', '1225427677895213057', '1237952339959107585');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237952502639382529', '1225427677895213057', '1237952502589050881');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237952663709044737', '1225427677895213057', '1237952663654518785');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237953401860411393', '1225427677895213057', '1237953401772331009');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237953594144083970', '1225427677895213057', '1237953594093752321');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237967795608641538', '1225427677895213057', '1237967795361177601');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237967907206488065', '1225427677895213057', '1237967907151962114');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237967962277699585', '1225427677895213057', '1237967962218979329');
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`) VALUES ('1237968014840717313', '1225427677895213057', '1237968014773608449');

DROP TABLE IF EXISTS `sys_user_info`;
CREATE TABLE `sys_user_info` (
  `id` bigint(20) NOT NULL COMMENT '主键',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户名',
  `password` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '密码',
  `user_portrait` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '用户头像',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '所属部门Id',
  `dept_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '所属部门名称',
  `lock_flag` tinyint(1) DEFAULT NULL COMMENT '锁定标记：{ -1:锁定,0:正常 }',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='系统用户信息表';

INSERT INTO `sys_user_info` (`id`, `username`, `password`, `user_portrait`, `dept_id`, `dept_name`, `lock_flag`) VALUES ('10001', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png', '0', NULL, '0');

DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户Id',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户角色关联表';

INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`) VALUES ('1238462333884420099', '10001', '1225427677895213057');

