package com.wft.design.upms.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wft.design.common.mybatis.service.impl.BaseServiceImpl;
import com.wft.design.upms.entity.SysDept;
import com.wft.design.upms.mapper.SysDeptMapper;
import com.wft.design.upms.service.SysDeptService;
import com.wft.design.upms.service.SysUserInfoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDateTime;

/**
 * @author WFT
 * @date 2020/3/13
 * description:
 */
@Service
@RequiredArgsConstructor
public class SysDeptServiceImpl extends BaseServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    private final SysUserInfoService userInfoService;

    @Override
    public void updateDept(SysDept dept) {
        int count = this.count(
                Wrappers.<SysDept>lambdaQuery()
                        .eq(SysDept::getDeptName, dept.getDeptName())
                        .notIn(SysDept::getId,dept.getId())
        );
        Assert.isTrue(count == 0,"部门名称已存在");
        dept.setLastModifyTime(LocalDateTime.now());
        //  修改部门信息
        if (this.updateById(dept)){
            //  修改与部门关联的用户信息
            this.userInfoService.updateDept(dept);
        }
    }

    @Override
    public void deleteDeptById(Long deptId) {
        if (this.removeById(deptId)){
            this.userInfoService.deleteDept(deptId);
        }
    }
}
