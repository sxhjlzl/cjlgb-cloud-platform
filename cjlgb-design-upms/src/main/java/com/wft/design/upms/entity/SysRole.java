package com.wft.design.upms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.wft.design.common.core.bean.BaseEntity;
import com.wft.design.common.core.validation.AddValidation;
import com.wft.design.common.core.validation.EditValidation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author WFT
 * @date 2020/2/6
 * description: 系统角色
 */
@Getter
@Setter
@NoArgsConstructor
public class SysRole extends BaseEntity {

    /**
     * 名称
     */
    @NotBlank(message = "角色名称不能为空",groups = { AddValidation.class, EditValidation.class })
    private String roleName;

    /**
     * 描述
     */
    private String roleDescribe;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime lastModifyTime;

    /**
     * 菜单Id列表
     */
    @TableField(exist = false)
    private List<Long> menuIdList;
}
