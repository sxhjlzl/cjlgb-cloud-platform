package com.wft.design.upms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wft.design.common.core.bean.BaseEntity;
import com.wft.design.common.core.validation.AddValidation;
import com.wft.design.common.core.validation.EditValidation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * @author WFT
 * @date 2020/1/2
 * description: 系统用户信息
 */
@Getter
@Setter
@NoArgsConstructor
public class SysUserInfo extends BaseEntity {

    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空",groups = { AddValidation.class, EditValidation.class })
    private String username;

    /**
     * 密码
     * 由明文密码进行Md5 + 随机盐再Md5
     */
    @JsonIgnore
    private String password;

    /**
     * 用户头像
     */
    private String userPortrait;

    /**
     * 所属部门Id
     */
    private Long deptId;

    /**
     * 所属部门名称
     */
    private String deptName;

    /**
     * 锁定标记：{ -1:锁定,0:正常 }
     */
    private Integer lockFlag;

    /**
     * 角色Id列表
     */
    @TableField(exist = false)
    @NotEmpty(message = "角色Id不能为空",groups = { AddValidation.class })
    private List<Long> roleIds;

}
