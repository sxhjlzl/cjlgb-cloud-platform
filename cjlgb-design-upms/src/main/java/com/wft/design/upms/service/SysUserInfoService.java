package com.wft.design.upms.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wft.design.common.mybatis.service.BaseService;
import com.wft.design.upms.constant.BasicData;
import com.wft.design.upms.entity.SysDept;
import com.wft.design.upms.entity.SysUserInfo;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

/**
 * @author WFT
 * @date 2020/1/3
 * description:
 */
public interface SysUserInfoService extends BaseService<SysUserInfo> {

    /**
     * 修改部门名称
     * @param dept com.wft.design.upms.entity.SysDept
     */
    default void updateDept(SysDept dept){
        this.update(
                Wrappers.<SysUserInfo>lambdaUpdate()
                        .set(SysUserInfo::getDeptName,dept.getDeptName())
                        .eq(SysUserInfo::getDeptId,dept.getId())
        );
    }

    /**
     * 删除部门
     * @param deptId java.lang.Long
     */
    default void deleteDept(Long deptId){
        this.update(
                Wrappers.<SysUserInfo>lambdaUpdate()
                        .set(SysUserInfo::getDeptId,null)
                        .set(SysUserInfo::getDeptName,null)
                        .eq(SysUserInfo::getDeptId,deptId)
        );
    }

    /**
     * 根据账号Id获取账号信息
     * @param accountId java.lang.Long
     * @return com.wft.design.upms.entity.SysUserInfo
     */
    default SysUserInfo selectById(Long accountId){
        return this.getById(accountId);
    }

    /**
     * 根据用户名获取账号信息
     * @param username 用户名
     * @return com.wft.design.upms.entity.SysUserInfo
     */
    default SysUserInfo selectByUsername(String username){
        return this.getOne(Wrappers.<SysUserInfo>lambdaQuery().eq(SysUserInfo::getUsername,username));
    }

    /**
     * 明文密码加密
     * @param plaintext 明文密码
     * @param salt 随机盐
     * @return java.lang.String
     */
    default String encryption(String plaintext, String salt){
        plaintext += salt;
        return DigestUtils.md5DigestAsHex(plaintext.getBytes());
    }

    /**
     * 分页查询用户列表
     * @param page com.baomidou.mybatisplus.core.metadata.IPage<SysUserInfo>
     * @param params com.wft.design.upms.entity.SysUserInfo
     * @return com.baomidou.mybatisplus.core.metadata.IPage<SysUserInfo>
     */
    default IPage<SysUserInfo> pagingQuery(IPage<SysUserInfo> page, SysUserInfo params){
        LambdaQueryWrapper<SysUserInfo> wrapper = Wrappers.lambdaQuery();
        //  过滤系统账号
        wrapper.notIn(SysUserInfo::getId, BasicData.USER_ID);
        if (null != params){
            //  根据锁定标记查询
            if (null != params.getLockFlag()){
                wrapper.eq(SysUserInfo::getLockFlag,params.getLockFlag());
            }
            //  根据部门Id查询
            if (null != params.getDeptId() && params.getDeptId() != 0){
                wrapper.eq(SysUserInfo::getDeptId,params.getDeptId());
            }
            //  根据用户名查询模糊查询
            if (StringUtils.hasText(params.getUsername())){
                wrapper.likeRight(SysUserInfo::getUsername,params.getUsername());
            }
        }
        return this.page(page,wrapper);
    }

    /**
     * 创建系统用户
     * @param params com.wft.design.upms.entity.SysUserInfo
     */
    void create(SysUserInfo params);

    /**
     * 删除用户
     * @param userId java.lang.Long
     */
    void delete(Long userId);
}
