package com.wft.design.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.upms.entity.SysRole;

/**
 * @author WFT
 * @date 2020/2/6
 * description:
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {



}
