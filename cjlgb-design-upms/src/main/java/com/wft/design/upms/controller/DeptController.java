package com.wft.design.upms.controller;

import com.wft.design.common.core.validation.AddValidation;
import com.wft.design.common.core.validation.EditValidation;
import com.wft.design.common.security.annotation.PreAuthorize;
import com.wft.design.upms.entity.SysDept;
import com.wft.design.upms.service.SysDeptService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author WFT
 * @date 2020/3/13
 * description: 部门-控制器
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/dept")
public class DeptController {

    private final SysDeptService deptService;

    /**
     * 获取部门列表
     * @return java.util.List<SysDept>
     */
    @GetMapping(value = "/list")
    @PreAuthorize
    public List<SysDept> getList(){
        return this.deptService.list();
    }

    /**
     * 添加部门
     * @param dept com.wft.design.upms.entity.SysDept
     * @return com.wft.design.upms.entity.SysDept
     */
    @PostMapping
    @PreAuthorize
    public SysDept add(@Validated(value = AddValidation.class) @RequestBody SysDept dept){
        return this.deptService.createDept(dept);
    }

    /**
     * 修改部门
     * @param dept com.wft.design.upms.entity.SysDept
     */
    @PutMapping
    @PreAuthorize
    public void edit(@Validated(value = EditValidation.class) @RequestBody SysDept dept){
        this.deptService.updateDept(dept);
    }

    /**
     * 删除部门
     * @param deptId 部门Id
     */
    @DeleteMapping(value = "/{deptId}")
    @PreAuthorize
    public void delete(@PathVariable(value = "deptId") Long deptId){
        this.deptService.deleteDeptById(deptId);
    }

}
