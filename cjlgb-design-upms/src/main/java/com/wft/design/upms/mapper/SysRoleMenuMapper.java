package com.wft.design.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.upms.entity.SysRoleMenu;

/**
 * @author WFT
 * @date 2020/3/10
 * description:
 */
public interface SysRoleMenuMapper extends BaseMapper<SysRoleMenu> {


}
