package com.wft.design.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.upms.entity.SysUserInfo;

/**
 * @author WFT
 * @date 2020/1/3
 * description:
 */
public interface SysUserInfoMapper extends BaseMapper<SysUserInfo> {


}
