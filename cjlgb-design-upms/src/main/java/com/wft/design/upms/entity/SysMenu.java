package com.wft.design.upms.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.wft.design.common.core.bean.BaseEntity;
import com.wft.design.common.core.util.TreeUtils;
import com.wft.design.common.core.validation.AddValidation;
import com.wft.design.common.core.validation.EditValidation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author WFT
 * @date 2020/2/7
 * description: 系统菜单
 */
@Setter
@Getter
@NoArgsConstructor
public class SysMenu extends BaseEntity implements TreeUtils.TreeEntity<SysMenu> {

    /**
     * 菜单名称
     */
    @NotBlank(message = "名称不能为空",groups = { AddValidation.class, EditValidation.class })
    private String name;

    /**
     * 权限标识
     */
    @NotBlank(message = "唯一标识不能为空",groups = { AddValidation.class, EditValidation.class })
    private String permission;

    /**
     * 索引
     */
    @JsonIgnore
    private String indexes;

    /**
     * 图标
     */
    private String icon;

    /**
     * 前端页面路径
     */
    private String path;

    /**
     * 父级菜单Id
     */
    @NotNull(message = "父级Id不能为空",groups = { AddValidation.class, EditValidation.class })
    private Long pid;

    /**
     * 排序字段
     */
    private Integer sort;

    /**
     * 类型：{ 1：菜单，2：按钮 }
     */
    @NotNull(message = "唯一标识不能为空",groups = { AddValidation.class, EditValidation.class })
    private Integer type;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime lastModifyTime;

    /* -------------------------------------------------------------------------------------------------------------- */

    /**
     * 子节点列表
     */
    @TableField(exist = false)
    private List<SysMenu> children;
}
