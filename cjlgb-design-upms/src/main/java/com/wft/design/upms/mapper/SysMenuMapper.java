package com.wft.design.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.upms.entity.SysMenu;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author WFT
 * @date 2020/2/7
 * description:
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

    /**
     * 根据子菜单Id获取上级菜单的Id列表
     * @param childrenIds 子菜单Id列表
     * @return java.util.List<Long>
     */
    @Select(value = " " +
            "WITH RECURSIVE tmp0 as " +
            "( " +
            "   SELECT id,pid FROM sys_menu WHERE id IN (${childrenIds})" +
            "   UNION" +
            "   SELECT m.id,m.pid FROM sys_menu m, tmp0 t WHERE 1=1 AND m.id=t.pid" +
            ") " +
            "SELECT id FROM tmp0")
    List<Long> selectIdsByChildrenIds(@Param(value = "childrenIds") String childrenIds);

    /**
     * 查询同级菜单中最大的排序值
     * @param pid java.lang.Long
     * @param id java.lang.Long
     * @return java.lang.Integer
     */
    @Select(value = "SELECT MAX(sort) FROM sys_menu WHERE pid = #{pid} AND id <> #{id}")
    Integer selectMaxSortByPid(@Param(value = "pid") Long pid,@Param(value = "id") Long id);

    /**
     * 查询同级菜单中排序值大于或等于指定排序值的菜单列表
     * @param menu com.wft.design.upms.entity.SysMenu
     * @return java.util.Collection<SysMenu>
     */
    @Select(value = "SELECT * FROM sys_menu WHERE id <> #{menu.id} AND pid = #{menu.pid} AND sort >= #{menu.sort}")
    List<SysMenu> selectGeListByPidAndSort(@Param(value = "menu") SysMenu menu);

}
