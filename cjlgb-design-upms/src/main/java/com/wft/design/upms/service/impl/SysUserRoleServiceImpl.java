package com.wft.design.upms.service.impl;

import com.wft.design.common.mybatis.service.impl.BaseServiceImpl;
import com.wft.design.upms.entity.SysUserRole;
import com.wft.design.upms.mapper.SysUserRoleMapper;
import com.wft.design.upms.service.SysUserRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author WFT
 * @date 2020/3/13
 * description:
 */
@Service
@RequiredArgsConstructor
public class SysUserRoleServiceImpl extends BaseServiceImpl<SysUserRoleMapper, SysUserRole>
        implements SysUserRoleService {



}
