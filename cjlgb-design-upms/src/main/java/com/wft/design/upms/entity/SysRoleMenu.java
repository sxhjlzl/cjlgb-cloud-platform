package com.wft.design.upms.entity;

import com.wft.design.common.core.bean.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author WFT
 * @date 2020/3/10
 * description: 系统角色菜单关联
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class SysRoleMenu extends BaseEntity {

    /**
     * 角色Id
     */
    private Long roleId;

    /**
     * 菜单Id
     */
    private Long menuId;

}
