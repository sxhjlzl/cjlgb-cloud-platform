package com.wft.design.upms.handler;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wft.design.upms.channel.SysMenuChannel;
import com.wft.design.upms.constant.BasicData;
import com.wft.design.upms.entity.SysMenu;
import com.wft.design.upms.entity.SysRoleMenu;
import com.wft.design.upms.mapper.SysMenuMapper;
import com.wft.design.upms.service.SysRoleMenuService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author WFT
 * @date 2020/3/10
 * description: 系统菜单管道处理器
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class SysMenuChannelHandler {

    private final SysRoleMenuService roleMenuService;

    private final SysMenuMapper menuMapper;

    /**
     * 监听菜单被删除
     * @param menu com.wft.design.upms.entity.SysMenu
     */
    @StreamListener(value = SysMenuChannel.INPUT,condition = "headers['method']=='del'")
    public void listenerDelete(SysMenu menu){
        //  获取当前菜单的子菜单Id列表
        Wrapper<SysMenu> wrapper = Wrappers.<SysMenu>lambdaQuery().likeRight(SysMenu::getIndexes, menu.getIndexes());
        List<Long> menuIds = this.menuMapper.selectList(wrapper).stream().map(SysMenu::getId).collect(Collectors.toList());
        menuIds.add(menu.getId());
        //  删除此菜单与角色间的绑定关系
        this.roleMenuService.remove(Wrappers.<SysRoleMenu>lambdaQuery().in(SysRoleMenu::getMenuId,menuIds));
        //  重新设置同级菜单的排序值
        this.listenerEdit(menu);
        //  删除此菜单下的子菜单
        this.menuMapper.deleteBatchIds(menuIds);
    }

    /**
     * 监听菜单被修改
     * @param menu com.wft.design.upms.entity.SysMenu
     */
    @StreamListener(value = SysMenuChannel.INPUT,condition = "headers['method']=='edit'")
    public void listenerEdit(SysMenu menu){
        //  重新设置同级菜单的排序值
        if (menu.getSort().equals(Integer.MAX_VALUE)){
            //  获取同级菜单最大的排序值
            Integer maxSort = this.menuMapper.selectMaxSortByPid(menu.getPid(),menu.getId());
            maxSort = null == maxSort ? 1 : ++maxSort;
            menu.setSort(maxSort);
            this.menuMapper.updateById(menu);
        } else {
            //  获取同级菜单排序值大于当前菜单的菜单列表
            List<SysMenu> menuList = this.menuMapper.selectGeListByPidAndSort(menu);
            if (null != menuList){
                int sort = menu.getSort() + 1;
                //  重新设置排序值
                for (int i = 0; i < menuList.size(); i++){
                    menuList.get(i).setSort(sort + i);
                    this.menuMapper.updateById(menuList.get(i));
                }
            }
        }
    }

    /**
     * 监听菜单新增
     * @param menu com.wft.design.upms.entity.SysMenu
     */
    @StreamListener(value = SysMenuChannel.INPUT,condition = "headers['method']=='add'")
    public void listenerAdd(SysMenu menu){
        //  将此菜单的访问操作权限授权于最大角色
        this.roleMenuService.save(new SysRoleMenu(BasicData.ROLE_ID,menu.getId()));
        //  设置索引字段
        if (!menu.getPid().equals(0L)){
            SysMenu parentMenu = this.menuMapper.selectOne(Wrappers.<SysMenu>lambdaQuery().eq(SysMenu::getId, menu.getPid()));
            menu.setIndexes(parentMenu.getIndexes() + menu.getId() + "/");
        } else {
            menu.setIndexes(menu.getId() + "/");
        }
        this.menuMapper.updateById(menu);
        //  重新设置同级菜单的排序值
        this.listenerEdit(menu);
    }

}
