package com.wft.design.upms.entity;

import com.wft.design.common.core.bean.BaseEntity;
import com.wft.design.common.core.validation.AddValidation;
import com.wft.design.common.core.validation.EditValidation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author WFT
 * @date 2020/3/13
 * description: 系统部门
 */
@Getter
@Setter
@NoArgsConstructor
public class SysDept extends BaseEntity {

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空",groups = { AddValidation.class, EditValidation.class })
    private String deptName;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime lastModifyTime;

}
