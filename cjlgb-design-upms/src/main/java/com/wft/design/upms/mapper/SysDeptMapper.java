package com.wft.design.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.upms.entity.SysDept;

/**
 * @author WFT
 * @date 2020/3/13
 * description:
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {


}
