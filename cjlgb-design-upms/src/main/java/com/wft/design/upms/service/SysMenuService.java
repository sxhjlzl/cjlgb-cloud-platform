package com.wft.design.upms.service;

import com.wft.design.upms.entity.SysMenu;

import java.util.Collection;
import java.util.List;

/**
 * @author WFT
 * @date 2020/2/7
 * description: 系统菜单CURD接口
 */
public interface SysMenuService {

    /**
     * 获取菜单列表
     * @return java.util.List<SysMenu>
     */
    List<SysMenu> selectList();

    /**
     * 根据Id获取菜单信息
     * @param menuId java.lang.Long
     * @return com.wft.design.upms.entity.SysMenu
     */
    SysMenu selectById(Long menuId);

    /**
     * 创建菜单
     * @param menu com.wft.design.upms.entity.SysMenu
     */
    void createMenu(SysMenu menu);

    /**
     * 根据Id修改菜单信息
     * @param menu com.wft.design.upms.entity.SysMenu
     */
    void updateMenu(SysMenu menu);

    /**
     * 根据Id删除菜单信息
     * @param menuId java.lang.Long
     */
    void deleteMenuById(Long menuId);

    /**
     * 根据pid查询菜单列表
     * @param pid java.lang.Long
     * @param authorities java.util.Collection<String>
     * @return java.util.List<SysMenu>
     */
    List<SysMenu> selectMenuListByPid(Long pid, Collection<String> authorities);

}
