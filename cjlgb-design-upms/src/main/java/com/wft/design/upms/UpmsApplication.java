package com.wft.design.upms;

import com.wft.design.upms.channel.SysMenuChannel;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

/**
 * @author WFT
 * @date 2020/1/2
 * description: Upms服务启动类
 */
@SpringCloudApplication
@EnableBinding(value = SysMenuChannel.class)
@MapperScan(value = "com.wft.design.upms.mapper")
public class UpmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(UpmsApplication.class,args);
    }

}
