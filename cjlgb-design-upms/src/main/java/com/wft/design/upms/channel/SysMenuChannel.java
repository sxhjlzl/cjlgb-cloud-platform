package com.wft.design.upms.channel;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author WFT
 * @date 2020/3/10
 * description: 系统菜单管道
 */
public interface SysMenuChannel {

    String INPUT = "sys_menu_channel_input";

    /**
     * 订阅通道
     * @return org.springframework.messaging.SubscribableChannel
     */
    @Input(value = INPUT)
    SubscribableChannel input();

    /**
     * 消息通道
     * @return org.springframework.messaging.MessageChannel
     */
    @Output(value = "sys_menu_channel_output")
    MessageChannel output();

}
