package com.wft.design.upms.controller;

import com.wft.design.common.core.validation.AddValidation;
import com.wft.design.common.core.validation.EditValidation;
import com.wft.design.common.security.annotation.PreAuthorize;
import com.wft.design.upms.entity.SysRole;
import com.wft.design.upms.service.SysRoleMenuService;
import com.wft.design.upms.service.SysRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author WFT
 * @date 2020/2/6
 * description: 系统角色控制器
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/role")
public class RoleController {

    private final SysRoleService roleService;
    private final SysRoleMenuService roleMenuService;

    /**
     * 获取角色列表
     * @param role 查询条件
     * @return java.util.List<SysRole>
     */
    @GetMapping
    @PreAuthorize
    public List<SysRole> list(SysRole role){
        return this.roleService.selectList(role);
    }

    /**
     * 添加角色
     * @param role 角色信息
     */
    @PostMapping
    @PreAuthorize
    public void add(@Validated(value = AddValidation.class) @RequestBody SysRole role){
        this.roleService.createRole(role);
    }

    /**
     * 根据Id获取角色信息
     * @param roleId 角色Id
     * @return com.wft.design.upms.entity.SysRole
     */
    @GetMapping(value = "/{roleId}")
    @PreAuthorize
    public SysRole get(@PathVariable(value = "roleId")Long roleId){
        return this.roleService.selectById(roleId);
    }

    /**
     * 根据Id修改角色信息
     * @param role com.wft.design.upms.entity.SysRole
     */
    @PutMapping
    @PreAuthorize
    public void edit(@Validated(value = EditValidation.class) @RequestBody SysRole role){
        this.roleService.updateRole(role);
    }

    /**
     * 根据Id删除角色
     * @param roleId java.lang.Long
     */
    @DeleteMapping(value = "/{roleId}")
    @PreAuthorize
    public void delete(@PathVariable(value = "roleId")Long roleId){
        this.roleService.deleteRole(roleId);
    }

    /**
     * 获取角色的权限列表
     * @param roleId java.lang.Long
     * @return java.util.List<Long>
     */
    @GetMapping(value = "/authorities/{roleId}")
    @PreAuthorize
    public List<Long> getAuthorities(@PathVariable(value = "roleId") Long roleId){
        return this.roleMenuService.selectMenuIdListByRoleId(roleId);
    }

    /**
     * 修改角色权限列表
     * @param role com.wft.design.upms.entity.SysRole
     */
    @PutMapping(value = "/authorities")
    @PreAuthorize
    public void editAuthorities(@RequestBody SysRole role){
        Assert.notNull(role.getId(),"角色Id不能为空");
        Assert.notNull(role.getMenuIdList(),"菜单Id列表不能为空");
        this.roleMenuService.updateMenuListByRoleId(role);
    }

}
