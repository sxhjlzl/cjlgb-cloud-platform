package com.wft.design.upms.service;

import com.wft.design.common.mybatis.service.BaseService;
import com.wft.design.upms.entity.SysRole;
import com.wft.design.upms.entity.SysRoleMenu;

import java.util.List;

/**
 * @author WFT
 * @date 2020/3/13
 * description:
 */
public interface SysRoleMenuService extends BaseService<SysRoleMenu> {

    /**
     * 获取某个角色的权限列表
     * @param roleId java.lang.Long
     * @return java.util.List<Long>
     */
    List<Long> selectMenuIdListByRoleId(Long roleId);

    /**
     * 修改角色的权限列表
     * @param role com.wft.design.upms.entity.SysRole
     */
    void updateMenuListByRoleId(SysRole role);

}
