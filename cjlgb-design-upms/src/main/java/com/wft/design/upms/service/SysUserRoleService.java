package com.wft.design.upms.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.IService;
import com.wft.design.upms.entity.SysUserRole;

/**
 * @author WFT
 * @date 2020/3/13
 * description:
 */
public interface SysUserRoleService extends IService<SysUserRole> {

    /**
     * 删除用户与角色间的关联
     * @param userId java.lang.Long
     */
    default void deleteByUserId(Long userId){
        this.remove(Wrappers.<SysUserRole>lambdaQuery().eq(SysUserRole::getUserId,userId));
    }

}
