package com.wft.design.upms.service;

import com.wft.design.upms.entity.SysRole;

import java.util.List;

/**
 * @author WFT
 * @date 2020/2/6
 * description: 系统角色CURD接口
 */
public interface SysRoleService {

    /**
     * 查询角色列表
     * @param role 查询条件
     * @return java.util.List<SysRole>
     */
    List<SysRole> selectList(SysRole role);

    /**
     * 创建角色
     * @param role com.wft.design.upms.entity.SysRole
     */
    void createRole(SysRole role);

    /**
     * 根据角色Id获取角色信息
     * @param roleId java.lang.Long
     * @return com.wft.design.upms.entity.SysRole
     */
    SysRole selectById(Long roleId);

    /**
     * 根据Id修改角色信息
     * @param role com.wft.design.upms.entity.SysRole
     */
    void updateRole(SysRole role);

    /**
     * 根据Id删除角色
     * @param roleId java.lang.Long
     */
    void deleteRole(Long roleId);

}
