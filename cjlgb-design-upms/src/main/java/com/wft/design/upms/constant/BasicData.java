package com.wft.design.upms.constant;

/**
 * @author WFT
 * @date 2020/3/30
 * description: 基本数据
 */
public interface BasicData {

    /**
     * 系统角色Id
     */
    long ROLE_ID = 1225427677895213057L;

    /**
     * 系统用户Id
     */
    long USER_ID = 10001L;

}
