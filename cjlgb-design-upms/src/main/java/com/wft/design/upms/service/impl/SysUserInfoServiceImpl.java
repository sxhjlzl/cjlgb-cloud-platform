package com.wft.design.upms.service.impl;

import com.wft.design.common.mybatis.service.impl.BaseServiceImpl;
import com.wft.design.upms.entity.SysUserInfo;
import com.wft.design.upms.entity.SysUserRole;
import com.wft.design.upms.mapper.SysUserInfoMapper;
import com.wft.design.upms.service.SysUserInfoService;
import com.wft.design.upms.service.SysUserRoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * @author WFT
 * @date 2020/1/3
 * description:
 */
@Service
@RequiredArgsConstructor
public class SysUserInfoServiceImpl extends BaseServiceImpl<SysUserInfoMapper, SysUserInfo>
        implements SysUserInfoService {

    private final SysUserRoleService userRoleService;

    @Override
    public void create(SysUserInfo params) {
        params.setUserPortrait("https://gw.alipayobjects.com/zos/antfincdn/XAosXuNZyF/BiazfanxmamNRoxxVxka.png");
        params.setPassword(this.encryption("123456",""));
        params.setLockFlag(0);
        //  保存用户以及用户和角色的关联
        if (this.save(params)){
            List<SysUserRole> list = new ArrayList<>(params.getRoleIds().size());
            params.getRoleIds().forEach(roleId -> list.add(new SysUserRole(params.getId(),roleId)));
            this.userRoleService.saveBatch(list);
        }
    }

    @Override
    public void delete(Long userId) {
        SysUserInfo userInfo = this.selectById(userId);
        Assert.notNull(userInfo,"账号Id不存在");
        if (this.removeById(userId)){
            this.userRoleService.deleteByUserId(userId);
        }
    }
}
