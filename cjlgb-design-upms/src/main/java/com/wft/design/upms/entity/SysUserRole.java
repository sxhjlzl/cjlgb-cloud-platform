package com.wft.design.upms.entity;

import com.wft.design.common.core.bean.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author WFT
 * @date 2020/3/13
 * description: 用户角色关联
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SysUserRole extends BaseEntity {

    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 菜单Id
     */
    private Long roleId;

}
