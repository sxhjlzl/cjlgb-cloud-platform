package com.wft.design.upms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.upms.entity.SysUserRole;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author WFT
 * @date 2020/3/13
 * description:
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    /**
     * 查询某用户的权限列表
     * @param userId java.lang.Long
     * @return java.util.List<String>
     */
    @Select(value =
            "SELECT DISTINCT m.permission FROM sys_menu AS m " +
            "LEFT JOIN sys_role_menu AS rm ON (rm.menu_id = m.id) " +
            "LEFT JOIN sys_user_role AS ur ON (ur.role_id = rm.role_id) " +
            "WHERE ur.user_id = #{userId}" )
    List<String> selectAuthorities(@Param(value = "userId") Long userId);

}
