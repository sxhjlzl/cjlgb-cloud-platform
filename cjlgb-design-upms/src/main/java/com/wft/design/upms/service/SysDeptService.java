package com.wft.design.upms.service;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.wft.design.common.mybatis.service.BaseService;
import com.wft.design.upms.entity.SysDept;
import org.springframework.util.Assert;

import java.time.LocalDateTime;

/**
 * @author WFT
 * @date 2020/3/13
 * description: 系统部门-CURD接口
 */
public interface SysDeptService extends BaseService<SysDept> {

    /**
     * 创建部门
     * @param dept com.wft.design.upms.entity.SysDept
     * @return com.wft.design.upms.entity.SysDept
     */
    default SysDept createDept(SysDept dept){
        int count = this.count(Wrappers.<SysDept>lambdaQuery().eq(SysDept::getDeptName, dept.getDeptName()));
        Assert.isTrue(count == 0,"部门名称已存在");
        LocalDateTime now = LocalDateTime.now();
        dept.setCreateTime(now);
        dept.setLastModifyTime(now);
        this.save(dept);
        return dept;
    }

    /**
     * 修改部门
     * @param dept com.wft.design.upms.entity.SysDept
     */
    void updateDept(SysDept dept);

    /**
     * 删除部门
     * @param deptId java.lang.Long
     */
    void deleteDeptById(Long deptId);
}
