package com.wft.design.gateway.channel;

import com.wft.design.common.core.constant.GlobalExchange;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

/**
 * @author WFT
 * @date 2020/2/5
 * description: 动态路由订阅者管道
 */
public interface DynamicRouteChannel {

    /**
     * 订阅通道
     * @return org.springframework.messaging.SubscribableChannel
     */
    @Input(value = GlobalExchange.DYNAMIC_ROUTE)
    SubscribableChannel input();

}
