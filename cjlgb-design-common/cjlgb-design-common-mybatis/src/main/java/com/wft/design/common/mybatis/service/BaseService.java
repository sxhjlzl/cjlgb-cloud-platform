package com.wft.design.common.mybatis.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @author WFT
 * @date 2019/8/11
 * description: 公共服务层接口
 */
public interface BaseService<Entity> extends IService<Entity> {

}
