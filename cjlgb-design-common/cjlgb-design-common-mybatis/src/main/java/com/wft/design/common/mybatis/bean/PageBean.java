package com.wft.design.common.mybatis.bean;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author WFT
 * @date 2019/12/18
 * description: 分页对象
 */
@Setter
@Getter
@NoArgsConstructor
public class PageBean<T> extends Page<T> {

    public PageBean(long current, long size) {
        super(current, size);
    }

}
