package com.wft.design.common.mybatis.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wft.design.common.mybatis.service.BaseService;

/**
 * @author WFT
 * @date 2019/8/11
 * description: 公共服务层接口实现类
 */
public class BaseServiceImpl<Mapper extends BaseMapper<Entity>,Entity> extends ServiceImpl<Mapper,Entity>
        implements BaseService<Entity> {

}
