package com.wft.design.common.security.service;

import com.wft.design.common.security.exception.UnauthorizedException;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;

/**
 * @author WFT
 * @date 2019/12/18
 * description: 令牌存储策略接口
 */
public interface TokenStorageService {

    String BEARER = "Bearer";

    /**
     * 从请求中获取访问令牌,默认从请求头中获取
     * @param request 请求对象
     * @return String
     * @throws UnauthorizedException 401异常
     */
    default String getAccessToken(HttpServletRequest request) throws UnauthorizedException {
        String header = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (null == header || !header.startsWith(BEARER)){
            throw UnauthorizedException.build();
        }
        return header.replace(BEARER,"").trim();
    }

}
