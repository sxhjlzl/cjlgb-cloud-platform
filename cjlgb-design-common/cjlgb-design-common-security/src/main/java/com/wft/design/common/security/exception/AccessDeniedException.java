package com.wft.design.common.security.exception;

import com.wft.design.common.core.constant.HttpStatus;
import com.wft.design.common.core.exception.BizException;

/**
 * @author WFT
 * @date 2019/11/7
 * description: 403异常
 */
public class AccessDeniedException extends BizException {

    private AccessDeniedException(){
        super(HttpStatus.FORBIDDEN,"You don't have permission to access on this server.");
    }

    private final static AccessDeniedException ACCESS_DENIED_EXCEPTION = new AccessDeniedException();

    public static AccessDeniedException build(){
        return ACCESS_DENIED_EXCEPTION;
    }

}
