package com.wft.design.common.security.service;

import com.wft.design.common.security.bean.AccessToken;
import com.wft.design.common.security.bean.Authentication;
import com.wft.design.common.security.exception.AuthenticationException;

import java.util.Collection;

/**
 * @author WFT
 * @date 2019/12/30
 * description:
 */
public interface AccessTokenService {

    /**
     * 根据访问令牌获取凭证信息
     * @param token 访问令牌
     * @return com.wft.design.common.security.bean.Authentication
     * @throws AuthenticationException 407异常
     */
    Authentication getAuthentication(String token) throws AuthenticationException;

    /**
     * 生成访问令牌
     * @param authentication 凭证信息
     * @param refreshToken 刷新令牌
     * @return com.wft.design.common.security.bean.AccessToken
     */
    AccessToken generateAccessToken(Authentication authentication, String refreshToken);

    /**
     * 删除凭证信息
     * @param accessToken 访问令牌
     */
    void removeAccessToken(String accessToken);
}
