package com.wft.design.common.security.exception;

import com.wft.design.common.core.constant.HttpStatus;
import com.wft.design.common.core.exception.BizException;

/**
 * @author WFT
 * @date 2019/11/7
 * description: 407异常
 */
public class AuthenticationException extends BizException {

    public AuthenticationException(String message) {
        super(HttpStatus.CERT_OVERDUE, message);
    }
    
}
