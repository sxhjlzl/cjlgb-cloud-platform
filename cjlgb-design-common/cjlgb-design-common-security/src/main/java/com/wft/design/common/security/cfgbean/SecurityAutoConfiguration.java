package com.wft.design.common.security.cfgbean;

import com.wft.design.common.security.bean.Authentication;
import com.wft.design.common.security.service.TokenStorageService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author WFT
 * @date 2019/12/19
 * description: 权限组件自动配置类
 */
@Configuration
@ComponentScan(basePackages = {
        "com.wft.design.common.security.service" ,
        "com.wft.design.common.security.handler"
})
public class SecurityAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public TokenStorageService tokenStorageStrategy(){
        return new TokenStorageService(){};
    }


    @Bean
    public RedisTemplate<String, Authentication> authenticationRedisTemplate(RedisConnectionFactory factory){
        RedisTemplate<String, Authentication> template = new RedisTemplate<>();
        template.setConnectionFactory(factory);
        return template;
    }
}
