package com.wft.design.common.security.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author WFT
 * @date 2019/12/20
 * description: 访问令牌
 */
@Getter
@AllArgsConstructor
public class AccessToken {

    /**
     * 访问令牌
     */
    private String accessToken;

    /**
     * 刷新令牌
     */
    private String refreshToken;

}
