package com.wft.design.common.security.service.impl;

import com.wft.design.common.security.bean.AccessToken;
import com.wft.design.common.security.bean.Authentication;
import com.wft.design.common.security.exception.AuthenticationException;
import com.wft.design.common.security.service.AccessTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author WFT
 * @date 2019/12/30
 * description:
 */
@Service
@RestController
@RequiredArgsConstructor
public class AccessTokenServiceImpl implements AccessTokenService {

    /**
     * 缓存名称
     */
    private final static String CACHE_NAME = "online:user:%s";

    /**
     * 缓存有效期为30分钟
     */
    private final static int VALIDITY = 1000 * 60 * 30;

    private final RedisTemplate<String,Authentication> redisTemplate;

    @Override
    public Authentication getAuthentication(String token) throws AuthenticationException {
        String cacheKey = String.format(CACHE_NAME,token);
        //  从缓存中获取
        Authentication authentication = this.redisTemplate.opsForValue().get(cacheKey);
        if (null == authentication){
            throw new AuthenticationException("Please replace with a new certificate. Current certificate has expired.");
        }
        //  延长缓存的有效期
        this.redisTemplate.expire(cacheKey,VALIDITY,TimeUnit.MILLISECONDS);
        return authentication;
    }

    @Override
    public AccessToken generateAccessToken(Authentication authentication, String refreshToken) {
        //  生成访问令牌
        String token = UUID.randomUUID().toString();
        String cacheKey = String.format(CACHE_NAME,token);
        //  保存到Redis中
        this.redisTemplate.opsForValue().set(cacheKey,authentication,VALIDITY,TimeUnit.MILLISECONDS);
        return new AccessToken(token,refreshToken);
    }

    @Override
    public void removeAccessToken(String accessToken) {
        String cacheKey = String.format(CACHE_NAME,accessToken);
        this.redisTemplate.delete(cacheKey);
    }
}
