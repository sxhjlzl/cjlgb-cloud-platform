package com.wft.design.common.security.annotation;

import java.lang.annotation.*;

/**
 * @author WFT
 * @date 2019/12/18
 * description: 预授权权限注解
 */
@Inherited
@Documented
@Target(value = ElementType.METHOD)
@Retention(value = RetentionPolicy.RUNTIME)
public @interface PreAuthorize {

    /**
     * @return 权限标识列表
     */
    String[] value() default {};

}
