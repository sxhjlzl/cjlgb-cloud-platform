package com.wft.design.common.security.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collection;

/**
 * @author WFT
 * @date 2019/11/18
 * description: 账号的认证信息
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Authentication implements Serializable {

    /**
     * 用户Id
     */
    private Long accountId;

    /**
     * 账号的权限列表
     */
    private Collection<String> authorities;

}
