package com.wft.design.common.security.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * @author WFT
 * @date 2020/1/3
 * description:
 */
@Getter
@Setter
@NoArgsConstructor
public class LoginParameter implements java.io.Serializable {

    /**
     * 用户名，密码模式时必填
     */
    @NotBlank(message = "用户名不能为空")
    private String username;

    /**
     * 密码，密码模式时必填
     * 由明文密码进行Md5 + 随机盐再Md5
     */
    @NotBlank(message = "密码不能为空")
    private String password;

    /**
     * 随机盐,密码模式必填
     */
    @NotBlank(message = "随机盐不能为空")
    private String salt;

    /**
     * 认证时,当不为空时,将生产RefreshToken
     */
    @JsonProperty(value = "remember_me")
    private Integer rememberMe;
}
