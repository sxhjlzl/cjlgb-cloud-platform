package com.wft.design.common.security.exception;

import com.wft.design.common.core.constant.HttpStatus;
import com.wft.design.common.core.exception.BizException;

/**
 * @author WFT
 * @date 2019/11/7
 * description: 401异常
 */
public class UnauthorizedException extends BizException {

    private UnauthorizedException(){
        super(HttpStatus.UNAUTHORIZED,"Full authentication is required to access this resource.");
    }

    private final static UnauthorizedException UNAUTHORIZED_EXCEPTION = new UnauthorizedException();

    public static UnauthorizedException build(){
        return UNAUTHORIZED_EXCEPTION;
    }

}
