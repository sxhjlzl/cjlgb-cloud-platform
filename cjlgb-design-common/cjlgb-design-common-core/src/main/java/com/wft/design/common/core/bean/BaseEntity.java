package com.wft.design.common.core.bean;

import com.wft.design.common.core.validation.EditValidation;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * @author WFT
 * @date 2019/12/18
 * description: 公共实体类
 */
@Getter
@Setter
public class BaseEntity implements java.io.Serializable {

    @NotNull(message = "Id不能为空",groups = EditValidation.class)
    private Long id;

}
