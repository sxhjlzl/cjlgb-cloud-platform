package com.wft.design.common.core.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author WFT
 * @date 2019/12/18
 * description: 业务异常
 */
@Getter
@AllArgsConstructor
public class BizException extends RuntimeException {

    /**
     * Http状态码
     */
    private int code;

    /**
     * 异常消息
     */
    private String message;

}
