package com.wft.design.common.core.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @author WFT
 * @date 2019/12/18
 * description: Web工具类
 */
@SuppressWarnings(value = "all")
public class WebUtils {


    /**
     * 获取HttpServletRequest对象
     * @return request
     */
    public static HttpServletRequest getHttpServletRequest(){
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }

}
