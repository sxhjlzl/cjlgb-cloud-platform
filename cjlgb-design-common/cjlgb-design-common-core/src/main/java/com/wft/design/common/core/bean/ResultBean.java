package com.wft.design.common.core.bean;

import com.wft.design.common.core.constant.HttpStatus;
import lombok.Getter;
import lombok.Setter;

/**
 * @author WFT
 * @date 2019/12/18
 * description: 返回值类型
 */
@Getter
public class ResultBean<Data> {

    /**
     * 状态码
     */
    private int code;

    /**
     * 提示信息
     */
    private String msg;

    /**
     * 响应数据
     */
    @Setter
    private Data data;

    private ResultBean(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    private ResultBean(){
        this(HttpStatus.OK,null);
    }

    private ResultBean(Data data){
        this();
        this.data = data;
    }

    public static <T> ResultBean<T> ok(T data){
        return new ResultBean<>(data);
    }

    public static <T> ResultBean<T> build(int code, String msg){
        return new ResultBean<>(code,msg);
    }

}
