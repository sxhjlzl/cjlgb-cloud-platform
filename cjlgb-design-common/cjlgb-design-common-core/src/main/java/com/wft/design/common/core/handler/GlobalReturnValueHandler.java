package com.wft.design.common.core.handler;

import com.wft.design.common.core.bean.ResultBean;
import com.wft.design.common.core.util.JsonUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @author WFT
 * @date 2019/12/18
 * description: 全局返回值处理器
 */
public class GlobalReturnValueHandler implements HandlerMethodReturnValueHandler {

    @Override
    public boolean supportsReturnType(MethodParameter returnType) {
        Class<?> clazz = returnType.getContainingClass();
        returnType.getMethodAnnotation(ResponseBody.class);

        return clazz.isAnnotationPresent(RestController.class)
                || clazz.isAnnotationPresent(ResponseBody.class)
                || returnType.getMethodAnnotation(ResponseBody.class) != null;
    }

    @Override
    public void handleReturnValue(Object result, MethodParameter returnType,
                                  ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {
        // 标识请求是否已经在该方法内完成处理
        mavContainer.setRequestHandled(true);
        HttpServletResponse response = webRequest.getNativeResponse(HttpServletResponse.class);
        if (null != response) {
            response.setContentType("application/json;charset=UTF-8");
            PrintWriter printWriter = response.getWriter();
            printWriter.write(JsonUtils.toJson(ResultBean.ok(result)));
            printWriter.close();
        }
    }
}
