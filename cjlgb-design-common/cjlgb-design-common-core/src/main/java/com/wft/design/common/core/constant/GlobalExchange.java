package com.wft.design.common.core.constant;

/**
 * @author WFT
 * @date 2020/2/5
 * description: 全局交换机
 */
public interface GlobalExchange {

    /**
     * 动态路由交换机
     */
    String DYNAMIC_ROUTE = "dynamic_route";


}
