package com.wft.design.common.core.constant;

/**
 * @author WFT
 * @date 2020/2/5
 * description: 全局缓存
 */
public interface GlobalCacheKey {

    /**
     * 网关路由
     */
    String GATEWAY_ROUTE = "gateway_route";


}
