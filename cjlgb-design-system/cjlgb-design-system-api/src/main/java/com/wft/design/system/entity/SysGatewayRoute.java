package com.wft.design.system.entity;

import com.wft.design.common.core.bean.BaseEntity;
import com.wft.design.common.core.validation.AddValidation;
import com.wft.design.common.core.validation.EditValidation;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * @author WFT
 * @date 2020/2/5
 * description: 系统网关路由
 */
@Getter
@Setter
@NoArgsConstructor
public class SysGatewayRoute extends BaseEntity {

    /**
     * 路由名称
     */
    @NotBlank(message = "路由名称不能为空",groups = { AddValidation.class, EditValidation.class})
    private String routeName;

    /**
     * 谓词
     */
    @NotBlank(message = "请配置路由谓词",groups = { AddValidation.class, EditValidation.class})
    private String predicates;

    /**
     * 过滤器
     */
    @NotBlank(message = "请配置路由过滤器",groups = { AddValidation.class, EditValidation.class})
    private String filters;

    /**
     * 请求URI
     */
    @NotBlank(message = "请配置URI",groups = { AddValidation.class, EditValidation.class})
    private String uri;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 最后修改时间
     */
    private LocalDateTime lastModifyTime;

}
