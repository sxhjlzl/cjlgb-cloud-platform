package com.wft.design.system;

import com.wft.design.system.channel.DynamicRouteChannel;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;

/**
 * @author WFT
 * @date 2020/2/5
 * description: 系统服务启动类
 */
@SpringCloudApplication
@EnableBinding(value = DynamicRouteChannel.class)
@MapperScan(value = "com.wft.design.system.mapper")
public class SystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(SystemApplication.class,args);
    }

}
