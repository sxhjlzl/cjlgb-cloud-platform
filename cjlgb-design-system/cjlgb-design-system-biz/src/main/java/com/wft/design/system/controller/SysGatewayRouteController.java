package com.wft.design.system.controller;

import com.wft.design.common.core.validation.AddValidation;
import com.wft.design.common.core.validation.EditValidation;
import com.wft.design.common.security.annotation.PreAuthorize;
import com.wft.design.system.entity.SysGatewayRoute;
import com.wft.design.system.service.SysGatewayRouteService;
import lombok.RequiredArgsConstructor;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

/**
 * @author WFT
 * @date 2020/2/5
 * description: 系统网关控制器
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/gateway/route")
public class SysGatewayRouteController {

    private final SysGatewayRouteService gatewayRouteService;

    /**
     * 获取路由列表
     * @return java.util.Collection<SysGatewayRoute>
     */
    @GetMapping
    @PreAuthorize
    public Collection<SysGatewayRoute> list(){
        return this.gatewayRouteService.selectRoutes();
    }

    /**
     * 添加路由
     * @param route com.wft.design.system.entity.SysGatewayRoute
     */
    @PostMapping
    @PreAuthorize
    public void add(@Validated(value = AddValidation.class) @RequestBody SysGatewayRoute route){
        Assert.hasText(URI.create(route.getUri()).getScheme(),"URI参数不正确");
        this.gatewayRouteService.createRoute(route);
    }

    /**
     * 根据Id删除路由
     * @param routeId java.lang.String
     */
    @DeleteMapping(value = "/{routeId}")
    @PreAuthorize
    public void delete(@PathVariable(value = "routeId") String routeId){
        this.gatewayRouteService.deleteById(routeId);
    }

    /**
     * 根据Id获取路由信息
     * @param routeId java.lang.String
     * @return com.wft.design.system.entity.SysGatewayRoute
     */
    @GetMapping(value = "/{routeId}")
    @PreAuthorize
    public SysGatewayRoute get(@PathVariable(value = "routeId")String routeId){
        return this.gatewayRouteService.selectById(routeId);
    }

    /**
     * 修改路由信息
     * @param route com.wft.design.system.entity.SysGatewayRoute
     */
    @PutMapping
    @PreAuthorize
    public void edit(@Validated(value = EditValidation.class) @RequestBody SysGatewayRoute route){
        Assert.hasText(URI.create(route.getUri()).getScheme(),"URI参数不正确");
        this.gatewayRouteService.updateRoute(route);
    }

}
