package com.wft.design.system.service;

import com.wft.design.system.entity.SysGatewayRoute;

import java.util.Collection;

/**
 * @author WFT
 * @date 2020/2/5
 * description: 系统网关CURD接口
 */
public interface SysGatewayRouteService {

    /**
     * 获取所有路由配置
     * @return Collection
     */
    Collection<SysGatewayRoute> selectRoutes();

    /**
     * 创建路由
     * @param route 路由信息
     */
    void createRoute(SysGatewayRoute route);

    /**
     * 根据路由Id删除路由信息
     * @param routeId java.lang.String
     */
    void deleteById(String routeId);

    /**
     * 根据Id查询路由信息
     * @param routeId 路由Id
     * @return com.wft.design.system.entity.SysGatewayRoute
     */
    SysGatewayRoute selectById(String routeId);

    /**
     * 根据路由Id修改路由信息
     * @param route com.wft.design.system.entity.SysGatewayRoute
     */
    void updateRoute(SysGatewayRoute route);
}
