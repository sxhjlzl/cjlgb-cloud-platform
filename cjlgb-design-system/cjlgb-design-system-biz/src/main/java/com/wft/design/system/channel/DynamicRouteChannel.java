package com.wft.design.system.channel;

import com.wft.design.common.core.constant.GlobalExchange;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author WFT
 * @date 2020/2/5
 * description: 动态路由发布者管道
 */
public interface DynamicRouteChannel {


    /**
     * 消息通道
     * @return org.springframework.messaging.MessageChannel
     */
    @Output(value = GlobalExchange.DYNAMIC_ROUTE)
    MessageChannel output();
}
