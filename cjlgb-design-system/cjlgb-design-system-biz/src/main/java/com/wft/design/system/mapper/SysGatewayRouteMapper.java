package com.wft.design.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wft.design.system.entity.SysGatewayRoute;

/**
 * @author WFT
 * @date 2020/2/5
 * description:
 */
public interface SysGatewayRouteMapper extends BaseMapper<SysGatewayRoute> {


}
