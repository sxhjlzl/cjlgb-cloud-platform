package com.wft.design.system.handler;

import com.wft.design.system.service.SysGatewayRouteService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.context.WebServerInitializedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * @author WFT
 * @date 2020/2/5
 * description: 动态路由处理器
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class DynamicRouteHandler {

    private final SysGatewayRouteService routeService;

    /**
     * 异步初始化网关路由
     */
    @EventListener(value = WebServerInitializedEvent.class)
    public void initGatewayRoutes(){
        log.info("正在初始化网关路由配置");
        this.routeService.selectRoutes();
        log.info("网关路由配置初始化完成");
    }

}
